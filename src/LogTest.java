import org.apache.log4j.Logger;

public class LogTest {
	private static final Logger LOG = Logger.getLogger(LogTest.class);
	
	public static void main(String[] args) {
		LOG.info("test: this is an info");
		LOG.debug("test: this is a debug");
		LOG.error("test: this is an error"); 
	}
}
